package kaspi.controller;

import kaspi.model.Order;
import kaspi.repository.SortOrder;
import kaspi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrderController implements ApplicationController<Order> {

    @Autowired
    private OrderService orderService;

    @Autowired
    @Qualifier("orderValidator")
    private Validator orderValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(orderValidator);
    }

    @Override
    @GetMapping("/order-form")
    public String openForm(Model model) {
        model.addAttribute("action", "Create");
        model.addAttribute("path", "create");
        return "order_form";
    }

    @Override
    @GetMapping("/{id}")
    public String get(@PathVariable("id") Integer id, Model model) {
        Order order = orderService.retrieve(id);
        if (order == null)
            return "not_found";
        model.addAttribute("action", "Update");
        model.addAttribute("path", "update/" + id);
        model.addAttribute("order", order);
        return "order_form";
    }

    @Override
    @GetMapping
    public String retrieveAll(@RequestParam(
            required = false, name="sort") String sort, Model model) {
        List<Order> orders;
        if (sort == null)
            orders = orderService.retrieveAll();
        else switch (sort) {
            case "totalPrice":
                orders = orderService
                        .retrieveAllSortedByTotalPrice(SortOrder.ASC);
                break;
            case "-totalPrice":
                orders = orderService
                        .retrieveAllSortedByTotalPrice(SortOrder.DESC);
                break;
            default:
                orders = orderService.retrieveAll();
                break;
        }
        model.addAttribute("orders", orders);
        return "order_list";
    }

    @Override
    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("order") Order order,
                         BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors())
            return openForm(model);
        orderService.create(order);
        return "redirect:/orders";
    }

    @Override
    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") Integer id,
                         @Valid @ModelAttribute("order") Order order,
                         BindingResult bindingResult, Model model) {
        order.setId(id);
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", "Update");
            return "order_form";
        }
        if (orderService.retrieve(order.getId()) == null)
            return "not_found";
        orderService.update(order);
        return "redirect:/orders";
    }

    @Override
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        Order order = orderService.retrieve(id);
        if (order == null)
            return "not_found";
        orderService.delete(order);
        return "redirect:/orders";
    }
}
