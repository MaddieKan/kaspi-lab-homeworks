package kaspi.controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;

public interface ApplicationController<T extends Serializable> {

    public String openForm(Model model);

    public String get(@PathVariable Integer id, Model model);

    public String retrieveAll(@RequestParam String sort, Model model);

    public String create(T entity, BindingResult bindingResult, Model model);

    public String update(@PathVariable Integer id, T entity, BindingResult bindingResult, Model model);

    public String delete(@PathVariable Integer id, Model model);
}
