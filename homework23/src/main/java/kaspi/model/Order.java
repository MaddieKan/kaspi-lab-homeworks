package kaspi.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "shop_order")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "shop_order_id_seq",
            sequenceName = "shop_order_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "shop_order_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "client_name", nullable = false)
    private String clientName;

    @Column(name = "total_price", nullable = false)
    private Float totalPrice;
    private Integer number;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public Order() { }

    public Order(String clientName, Float totalPrice,
                 Integer number, LocalDate date) {
        this.clientName = clientName;
        this.totalPrice = totalPrice;
        this.number = number;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", totalPrice=" + totalPrice +
                ", number=" + number +
                ", date=" + date +
                '}';
    }
}
