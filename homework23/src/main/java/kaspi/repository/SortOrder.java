package kaspi.repository;

public enum SortOrder {
    ASC,
    DESC;
}
