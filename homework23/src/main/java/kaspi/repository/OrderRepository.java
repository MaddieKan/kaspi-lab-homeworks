package kaspi.repository;

import kaspi.model.Order;
import kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class OrderRepository implements Repository<Order> {
    private Session session;

    @Override
    public Session openSession() {
        session = HibernateUtil.obtainSessionFactory().openSession();
        return session;
    }

    @Override
    public void closeSession() {
        session.close();
    }

    @Override
    public void create(Order order) {
        executeInsideTransaction(session -> session.save(order));
    }

    @Override
    public Order retrieve(Integer id) {
        return session.get(Order.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> retrieveAll() {
        return session.createQuery(
                "FROM Order o ORDER BY o.id").list();

    }

    @Override
    public void update(Order order) {
        executeInsideTransaction(session -> session.update(order));
    }

    @Override
    public void delete(Order order) {
        executeInsideTransaction(session -> session.delete(order));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> retrieveAllSorted(String fieldName, SortOrder sortOrder) {
        return session.createQuery(
                String.format("FROM Order o ORDER BY o.%s %s",
                        fieldName, sortOrder)).list();
    }

    private void executeInsideTransaction(Consumer<Session> command) {
        Transaction transaction = session.beginTransaction();
        command.accept(session);
        transaction.commit();
        session.close();
    }
}
