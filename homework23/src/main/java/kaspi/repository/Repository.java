package kaspi.repository;

import org.hibernate.Session;
import java.io.Serializable;
import java.util.List;

public interface Repository<T extends Serializable> {

    public Session openSession();

    public void closeSession();

    public void create(T entity);

    public T retrieve(Integer id);

    public List<T> retrieveAll();

    public void update(T entity);

    public void delete(T entity);

    public List<T> retrieveAllSorted(String fieldName, SortOrder sortOrder);
}

