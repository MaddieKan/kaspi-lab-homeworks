package kaspi.service;

import kaspi.repository.Repository;
import kaspi.repository.OrderRepository;
import kaspi.model.Order;
import kaspi.repository.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private static Repository<Order> orderRepository;

    public OrderService() {
        orderRepository = new OrderRepository();
    }

    public void create(Order order) {
        orderRepository.openSession();
        orderRepository.create(order);
        orderRepository.closeSession();
    }

    public Order retrieve(Integer id) {
        orderRepository.openSession();
        Order order = orderRepository.retrieve(id);
        orderRepository.closeSession();
        return order;
    }

    public List<Order> retrieveAll() {
        orderRepository.openSession();
        List<Order> orders = orderRepository.retrieveAll();
        orderRepository.closeSession();
        return orders;
    }

    public void update(Order order) {
        orderRepository.openSession();
        orderRepository.update(order);
        orderRepository.closeSession();
    }

    public void delete(Order order) {
        orderRepository.openSession();
        orderRepository.delete(order);
        orderRepository.closeSession();
    }

    public List<Order> retrieveAllSortedByTotalPrice(
            SortOrder sortOrder) {
        orderRepository.openSession();
        List<Order> orders = orderRepository
                .retrieveAllSorted("totalPrice", sortOrder);
        orderRepository.closeSession();
        return orders;
    }
}
