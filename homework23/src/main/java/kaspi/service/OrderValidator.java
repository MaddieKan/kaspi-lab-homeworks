package kaspi.service;

import kaspi.model.Order;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class OrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Order.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Order order = (Order) target;
        if (order.getClientName() == null || order.getClientName()
                .replaceAll("\\s+","").length() == 0)
            errors.rejectValue("clientName", "clientName.value.null");
        if (order.getTotalPrice() == null || order.getTotalPrice() == 0)
            errors.rejectValue("totalPrice", "totalPrice.value.null");
        else if (order.getTotalPrice() < 0)
            errors.rejectValue("totalPrice", "totalPrice.value.negative");
        if (order.getNumber() == null)
            errors.rejectValue("number", "number.value.null");
        else if (order.getNumber() < 0)
            errors.rejectValue("number", "number.value.negative");
        else if (order.getNumber() != (int) order.getNumber())
            errors.rejectValue("number", "number.value.decimal");
        if (order.getDate() == null)
            errors.rejectValue("date", "date.value.null");
    }
}
