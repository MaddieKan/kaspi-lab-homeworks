CREATE TABLE shop_order (
    id SERIAL PRIMARY KEY,
    client_name VARCHAR(100) NOT NULL,
    total_price REAL NOT NULL,
    number INTEGER UNIQUE NOT NULL,
    date DATE DEFAULT CURRENT_DATE
);