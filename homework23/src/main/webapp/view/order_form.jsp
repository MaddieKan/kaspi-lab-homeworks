<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Orders - ${action} Order</title>
</head>
<body>
<h3>${action} Order:</h3>
<form:form name="book_form" method="post"
      action="${path}" modelAttribute="order">
    <c:if test="${action == 'Update'}">
        <label for="id">ID:</label><br>
        <input readonly type="number" id="id" name="id" value="${order.id}"><br><br>
    </c:if>
    <label for="client_name">Client Name:</label><br>
    <input type="text" id="client_name" name="clientName" value="${order.clientName}">
    <form:errors path="clientName" cssStyle="color: red"/><br><br>
    <label for="total_price">Total Price:</label><br>
    <input type="number" id="total_price" name="totalPrice" step="0.01" value="${order.totalPrice}">
    <form:errors path="totalPrice" cssStyle="color: red"/><br><br>
    <label for="order_number">Order Number:</label><br>
    <input type="number" id="order_number" name="number" value="${order.number}">
    <form:errors path="number" cssStyle="color: red"/><br><br>
    <label for="date">Date:</label><br>
    <input type="date" id="date" name="date" value="${order.date}">
    <form:errors path="date" cssStyle="color: red"/><br><br>
    <input type="submit" value="Save">
</form:form><br>
<a href="<%=request.getContextPath()%>/orders">Go back to Orders</a>
</body>
</html>
