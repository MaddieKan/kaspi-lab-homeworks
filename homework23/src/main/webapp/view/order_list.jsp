<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Orders</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<h1>All Orders in Database:</h1>
<div>
    <button><a href="<%=request.getContextPath()%>/orders/order-form">
        Create New</a></button>
    <button><a href="<%=request.getContextPath()%>/orders?sort=totalPrice">
        Sort by Total Price Ascending</a></button>
    <button><a href="<%=request.getContextPath()%>/orders?sort=-totalPrice">
        Sort by Total Price Descending</a></button>
</div><br>
<table>
    <tr>
        <th>ID</th>
        <th>Client Name</th>
        <th>Total Price</th>
        <th>Order Number</th>
        <th>Date</th>
        <th>Update</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="order" items="${orders}">
        <tr>
            <td><c:out value="${order.id}"/></td>
            <td><c:out value="${order.clientName}"/></td>
            <td><c:out value="${order.totalPrice}"/></td>
            <td><c:out value="${order.number}"/></td>
            <td><c:out value="${order.date}"/></td>
            <td><button><a href="<%=request.getContextPath()%>/orders/${order.id}">
                Update</a></button></td>
            <td><button><a href="<%=request.getContextPath()%>/orders/delete/${order.id}">
                Delete</a></button></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
