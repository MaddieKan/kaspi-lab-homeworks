<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Form</title>
</head>
<body>
<h3>New Book:</h3>
<form name="book_form" method="post"
      action="http://localhost:8080/homework17/books">
    <label for="name">Name:</label><br>
    <input type="text" id="name" name="name"><br>
    <label for="author">Author:</label><br>
    <input type="text" id="author" name="author"><br>
    <label for="publisher">Publisher:</label><br>
    <input type="text" id="publisher" name="publisher"><br>
    <label for="year">Publication Year:</label><br>
    <input type="number" id="year" name="year"><br><br>
    <input type="submit" value="Add Book">
</form>
</body>
</html>