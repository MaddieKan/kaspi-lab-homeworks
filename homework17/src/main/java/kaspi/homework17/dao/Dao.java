package kaspi.homework17.dao;

import java.io.Serializable;
import java.util.List;
import jakarta.persistence.EntityManager;

public interface Dao<T extends Serializable> {

    public EntityManager createEntityManager();

    public void create(T entity);

    public List<T> retrieveAll();
}
