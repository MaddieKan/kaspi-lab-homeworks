package kaspi.homework17.services;

import kaspi.homework17.dao.BookDao;
import kaspi.homework17.dao.Dao;
import kaspi.homework17.entities.Book;

import java.util.List;

public class BookService {
    private static Dao<Book> bookDao;

    public BookService() {
        bookDao = new BookDao();
    }

    public void persist(Book book) {
        bookDao.create(book);
    }

    public List<Book> retrieveAll() {
        return bookDao.retrieveAll();
    }
}
