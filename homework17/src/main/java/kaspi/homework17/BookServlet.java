package kaspi.homework17;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import kaspi.homework17.entities.Book;
import kaspi.homework17.services.BookService;
import kaspi.homework17.utils.PersistenceUtil;

import java.io.IOException;

@WebServlet(name = "bookServlet", value = {"/books", "/books/add"})
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/books/add_book.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BookService bookService = new BookService();
        bookService.persist(new Book(request.getParameter("name"),
                request.getParameter("author"), request.getParameter("publisher"),
                Integer.parseInt(request.getParameter("year"))));

        request.setAttribute("books", bookService.retrieveAll());
//        PersistenceUtil.closeEntityManagerFactory();
        getServletContext().getRequestDispatcher("/books/book_list.jsp")
                .forward(request, response);
    }
}
