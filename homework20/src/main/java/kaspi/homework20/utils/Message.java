package kaspi.homework20.utils;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Message {
    NAME_NOT_PROVIDED("Name must not be empty"),
    INVALID_AGE("Age must be positive"),
    NOT_FOUND("Object with provided id does not exist"),
    SUCCESSFULLY_DELETED("Object has been successfully deleted");

    private final String content;

    Message(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return content;
    }
}
