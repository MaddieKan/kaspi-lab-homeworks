package kaspi.homework20.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import kaspi.homework20.entities.Employee;
import kaspi.homework20.utils.PersistenceUtil;

import java.util.List;
import java.util.function.Consumer;

public class EmployeeDao implements Dao<Employee> {
    private EntityManager entityManager;

    @Override
    public EntityManager createEntityManager() {
        entityManager = PersistenceUtil.obtainEntityManagerFactory().createEntityManager();
        return entityManager;
    }

    @Override
    public void closeEntityManager() {
        entityManager.close();
    }

    @Override
    public void create(Employee employee) {
        executeInsideTransaction(entityManager -> entityManager.persist(employee));
    }

    @Override
    public Employee retrieve(Integer id) {
        return entityManager.find(Employee.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Employee> retrieveAll() {
        return entityManager.createQuery(
                "SELECT e FROM Employee e ORDER BY e.id").getResultList();
    }

    @Override
    public Employee update(Employee employee) {
        entityManager.getTransaction().begin();
        if (entityManager.find(Employee.class, employee.getId()) == null)
            return null;
        Employee updated = entityManager.merge(employee);
        entityManager.getTransaction().commit();
        return updated;
    }

    @Override
    public boolean delete(Integer id) {
        Employee employee = retrieve(id);
        if (employee == null)
            return false;
        executeInsideTransaction(entityManager -> entityManager.remove(employee));
        return true;
    }

    private void executeInsideTransaction(Consumer<EntityManager> command) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        command.accept(entityManager);
        transaction.commit();
    }
}
