package kaspi.homework20;

import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import kaspi.homework20.entities.Employee;
import kaspi.homework20.services.EmployeeService;
import kaspi.homework20.utils.Message;

import java.net.URI;

@Path("/employees")
public class EmployeeResource {
    private EmployeeService employeeService = new EmployeeService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(@Context UriInfo uriInfo,
                           @Valid Employee employee) {
        if (employee.getName() == null)
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Message.NAME_NOT_PROVIDED).build();
        if (employee.getAge() <= 0)
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Message.INVALID_AGE).build();
        employeeService.create(employee);
        URI uri = UriBuilder.fromUri(uriInfo.getRequestUri())
                .path("{id}").build(employee.getId());
        return Response.created(uri).entity(employee).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {
        Employee employee = employeeService.retrieve(id);
        if (employee == null)
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(Message.NOT_FOUND).build();
        return Response.ok().entity(employee).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        return Response.ok().entity(employeeService
                .retrieveAll()).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Integer id,
                           @Valid Employee employee) {
        if (employee.getName() == null)
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Message.NAME_NOT_PROVIDED).build();
        if (employee.getAge() <= 0)
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Message.INVALID_AGE).build();
        employee.setId(id);
        if (employeeService.update(employee) == null)
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(Message.NOT_FOUND).build();
        return Response.ok().entity(employee).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        if (!employeeService.delete(id))
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(Message.NOT_FOUND).build();
        return Response.ok().entity(Message.SUCCESSFULLY_DELETED).build();
    }
}